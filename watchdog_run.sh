#!/bin/sh

name=${0##*\/}


## Source report.conf to obtain the notifycmd function 


host=`hostname | cut -f1 -d.`

# For files that should not be executed inside of /usr/local/bin/reports, manually exclude from eval_list 

eval_list=` find /usr/local/bin/reports -type f | grep -v $name$ | grep -v report.conf `


for RUN in $eval_list ; do
    eval $RUN ; exitstatus=$?
done
exit 0

